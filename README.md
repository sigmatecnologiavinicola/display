# README #

Visualizador del grado de la uva

### Instalacion del samba ###
* Instalacion del samba

Tal como dice en la web: https://www.raspberrypi.org/magpi/samba-file-server/

~~~
sudo apt-get update 
sudo apt-get upgrade 
sudo apt-get install samba samba-common-bin	
sudo mkdir -m 1777 /home/pi/samba
~~~

Ahora le añades la configuracion, abriendo y añadiendo en el siguiente ficheros, las lineas de abajo
~~~
sudo leafpad /etc/samba/smb.conf  o  sudo nano /etc/samba/smb.conf
~~~

~~~
[share]
Comment = Pi shared folder
Path = /home/pi/samba
Browseable = yes
Writeable = Yes
only guest = no
create mask = 0777
directory mask = 0777
Public = yes
Guest ok = yes
~~~

y añade el usuario de pi
~~~
sudo smbpasswd -a pi
~~~

### Como se instala ###
* Instalacion del paquete DisplayPuertoSerie
* Te lo descargas:
~~~ 
git clone https://pedro_valiente_verde@bitbucket.org/sigmatecnologiavinicola/display.git
~~~
* Entras a la carpeta: 
~~~
cd display
~~~
* Instalacion de las Dependencias de python flask y pyserial: 
~~~
pip3 install -r requirements.txt
sudo apt-get install x11-xserver-utils unclutter htop
~~~

* Ejecutas la instalacion, si lo queremos instalar en modo debug, a�adimos 1 al comando: .setup.sh 1 
~~~
./setup.sh
~~~
* Reinicias la raspberry
~~~
sudo reboot
~~~

### Utilizar alguna otra version/rama
Dentro de la carpeta del proyecto nos cambiamos a la rama, por ejemplo, si es a la rama Reestructuracion:
~~~ 
git fetch && git checkout Reestructuracion
~~~

