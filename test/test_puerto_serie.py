import unittest
from mock_puerto_serie import Patron, MockConexion
import display.puerto_serie as PS
import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

patrones_a_testear = [Patron("FM12.5FM323FM", "12.5"),
                    Patron("05.2FM05.3", "05.3"),
                    Patron("05.2FM05.3FM12.5", "05.3")]

conexion = MockConexion(patrones_a_testear)
puerto_serie = PS.PuertoSerie(conexion=conexion, log=logger)

class CheckRegex(unittest.TestCase):
    def test_numero_filtrado(self):
        while conexion.FinPatrones() == False:
            self.assertEqual(puerto_serie.Leer(), conexion.LecturaFiltrada())
        
        
if __name__ == "__main__":
    unittest.main()
