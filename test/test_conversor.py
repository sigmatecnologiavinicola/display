import unittest
from display.conversor  import Lectura


class MockPuertoSerie(object):
    def __init__(self,valor_lectura):
        self.valor_lectura=valor_lectura
        
    def Leer(self):
        return self.valor_lectura


class CheckLectura(unittest.TestCase):
    def test_lectura_fallida(self):
        puerto_serie = MockPuertoSerie('')        
        self.assertEqual( Lectura(puerto_serie), '')
        
    def test_volumen_baude(self):
        puerto_serie = MockPuertoSerie(15.2)        
        self.assertEqual( Lectura(puerto_serie), '14.3')
        
    def test_volumen_baude_texto(self):
        puerto_serie = MockPuertoSerie('15.2')        
        self.assertEqual( Lectura(puerto_serie), '14.3')
        
    def test_datos_no_numericos(self):
        puerto_serie = MockPuertoSerie('F15.2')        
        self.assertEqual( Lectura(puerto_serie), '')    
        
if __name__ == "__main__":
    unittest.main()