class Patron(object):
    def __init__(self, cadena_leida, lectura_a_realizar):
        self.cadena_leida = cadena_leida
        self.lectura_a_realizar = lectura_a_realizar
        
    def Lectura(self):
        return self.lectura_a_realizar
    
    def CadenaLeida(self):
        return self.cadena_leida

class MockConexion(object):
    '''
     Pequena clase que nos ayuda de el debugeo
    '''
    
    def __init__(self,patrones_a_testear):
        self.en_sepera = 1
        self.CODIGO_LEIDO = -3
        self.lectura_bruta = [Patron("FM12.5FM323FM", "12.5"), 
                              Patron("05.2FM05.3", "05.3"), 
                              Patron("05.2FM05.3FM12.5", "05.3")]
    
    def inWaiting(self):
        if self.en_sepera != self.CODIGO_LEIDO:
            self.numero_a_leer = ''
            
        self.en_sepera += 1            
        return self.en_sepera
    
    def read(self, size=1):
        # De esta manera probamos las excepciones
        self.en_sepera = self.CODIGO_LEIDO
        patron = self.lectura_bruta.pop()
        self.numero_a_leer = patron.Lectura()
        return patron.CadenaLeida()
    
    def LecturaFiltrada(self):
        return self.numero_a_leer
    
    def FinPatrones(self):
        return True if len(self.lectura_bruta) == 0 else False