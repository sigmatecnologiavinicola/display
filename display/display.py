# -*- coding: utf-8 -*-
from flask import Flask, jsonify, render_template
import argparse
import puerto_serie as PS
import logging
from logging.handlers import RotatingFileHandler
from conversor import Lectura

app = Flask(__name__)

parser = argparse.ArgumentParser("Display del grado")
parser.add_argument("-d", help="1 para pruebas", dest='debug', default=False, type=int)
parser.add_argument("-r", help="ruta de los logs", dest='ruta', default=".", type=str)
args = parser.parse_args()

logging.basicConfig(level=logging.DEBUG)

handle_error = RotatingFileHandler(args.ruta+"/log.error", maxBytes=5 * 1024 * 1024)
handle_error.setLevel(logging.ERROR)
handle_error.setFormatter(logging.Formatter('%(asctime)s - %(message)s')) 

handle_info = RotatingFileHandler(args.ruta+"/log.info", maxBytes=5 * 1024 * 1024)
handle_info.setLevel(logging.INFO)
handle_info.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))

# Es buena pratica que cada modulo, usasrlogger
logger = logging.getLogger(__name__)
logger.addHandler(handle_error)
logger.addHandler(handle_info)


puerto_serie = PS.PuertoSerie(log=logger, conexion=PS.EstablecerConexion(logger,args.debug))

@app.route('/_leer_puerto_serie')
def leer_puerto_serie():
    return jsonify(result=Lectura(puerto_serie))
    

@app.route('/')
def index():
    return render_template('index.html')



if __name__ == '__main__':
    logger.info("\\/Arranco el display \\/")
    app.run(host='localhost', port=8000, debug=False)
    logger.info("/\\ Se para el display /\\")
