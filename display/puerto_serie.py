'''
Created on 10 Jul 2017

@author: pedro

Se modela la conexion y el tratamiento de la maquina de grado
'''

import random    
import re
import time
import sys

class DebugConexion(object):
    '''
     Pequena clase que nos ayuda de el debugeo
    '''
    
    def __init__(self):
        self.en_sepera=1
    
    def inWaiting(self):
        self.en_sepera+=1
        return self.en_sepera
    
    def read(self, size=1):
        # De esta manera probamos las excepciones
        self.en_sepera=-2
        return "FM{}.{}FM".format(random.randint(10, 99), random.randint(0, 9))
    
reitentos=0  
puertos_disponibles=['/dev/ttyUSB0','/dev/ttyUSB1']  
def EstablecerConexion(log,debug=0):
    '''
    Metodo que nos devuelve la conexion a establecer
    '''
    global reitentos
    conexion=DebugConexion()
    
    if debug==0:
        conexion_fallida=1
        while conexion_fallida:
            try:                
                import serial
                conexion = serial.Serial(
                        port=puertos_disponibles[reitentos%2],
                        baudrate=1200,
                        parity=serial.PARITY_ODD,
                        stopbits=serial.STOPBITS_TWO,
                        bytesize=serial.SEVENBITS
                    )
                               
                conexion_fallida=0
            except serial.serialutil.SerialException as e:
                log.error("No se ha podido establecer la conexion al puerto serie, reintentamos ("+str(reitentos)+"), CAUSA: "+str(e))
                reitentos=reitentos+1
                time.sleep(1)
            else:
                log.info("Conexion establecida")
                conexion_fallida=0
        
    return conexion


class PuertoSerie(object):
    '''
    Configuracion del filtro
    '''
    SIN_LECTURA=''

    def __init__(self, log, conexion):
        '''
        Constructor
        '''
        self.conexion = conexion
        self.logger = log.getChild(__name__)       

            
            # self.conexion.isOpen()
    
    def Leer(self):
                
        lectura_bruta = self.SIN_LECTURA            
        # Devuelve el numero de bytes a la espera
        self.logger.debug("Leemos del puerto serie ...") 
        try:
            while self.conexion.inWaiting() > 0:
                lectura_bruta += self.conexion.read(1)                
        except IOError as e:
            self.logger.error("##### Conexion interrupida, intentamos reconectarnos")
            self.conexion=EstablecerConexion(self.logger,debug=0)
                  
        self.logger.debug("Filtramos el resultado ...") 
        m = re.search('(?<=FM)\d{1,3}\.\d{1}', lectura_bruta)
        
        valor_filtrado = ''
        try:
            if lectura_bruta!=self.SIN_LECTURA:
                valor_filtrado = m.group(0)
                self.logger.info("Lectura bruta: {}, Valor filtrado: {}".format(lectura_bruta, valor_filtrado)) 
        except AttributeError:
            self.logger.error("No se ha podido filtrar la cadena: {}".format(lectura_bruta))
        except:
            self.logger.error("Erro desconocido para la cadena: {}".format(lectura_bruta))
            
        
        return valor_filtrado
        
