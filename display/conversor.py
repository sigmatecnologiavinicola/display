
def EsConvertible2Float(cadena):
    try:
        float(cadena)
        return True
    except:
        return False

def GradoVolumen2Baume(grado_volumen):
    gradobrix = float(float(grado_volumen) + 2.0442) / 0.6757
    return gradobrix / 1.78

def Lectura(puerto_serie):
    grado_volumen = puerto_serie.Leer()
    resultado = '%.1f' % GradoVolumen2Baume(grado_volumen) if EsConvertible2Float(grado_volumen) else ''

    return resultado