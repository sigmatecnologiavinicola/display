#!/bin/bash

MODO_DEBUG=${1:-0}

# Directorio del script
SCRIPT_GIT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "Carpeta del script: "$SCRIPT_GIT

# Generamos el fichero
mkdir -p ~/.config/lxsession/LXDE-pi
cp -u --backup=numbered ~/.config/lxsession/LXDE-pi/autostart ~/.config/lxsession/LXDE-pi/autostart_backup
touch ~/.config/lxsession/LXDE-pi/autostart
echo "@lxpanel --profile LXDE-pi
@pcmanfm --desktop --profile LXDE-pi
#@xscreensaver -no-splash
@point-rpi
@xset s off
@xset -dpms
@xset s noblank

@sed -i 's/"exited_cleanly": false/"exited_cleanly": true/' ~/.config/chromium/Default/Preferences
@/usr/bin/python $SCRIPT_GIT"/display/display.py -d $MODO_DEBUG -r "$SCRIPT_GIT
@$SCRIPT_GIT/script/lanzador_chromium.sh" > ~/.config/lxsession/LXDE-pi/autostart
echo " ---- TERMINADO, REINICIE LA RASPI -----"

#reboot
